
function createNewUser() {
  let getFirstName = prompt('What is your name?');
  let getLastName = prompt('What is your lastname?');
  let newUser = {
    firstName: getFirstName,
    lastName: getLastName,
    getLogin() {
      let getFullName = this.firstName[0] + this.lastName;
      return getFullName.toLowerCase();
    }
  };
  console.log(newUser.getLogin());
  return newUser;

}
createNewUser()


// option 2

setTimeout(() => {
  function createNewUser() {
    const newUser = {};
    newUser.firstName = prompt("Please enter your first name:");
    newUser.lastName = prompt("Please enter your last name:");

    newUser.getLogin = function () {
      const firstLetter = newUser.firstName.charAt(0).toLowerCase();
      const login = firstLetter + newUser.lastName.toLowerCase();
      return login;
    }

    return newUser;
  }

  const user = createNewUser();
  console.log(user.getLogin());
}, 2000)